<?php 
/**
 * Plugin Name: WPezPlugins: Hubspot Newsletter Form AJAX Form
 * Plugin URI: https://gitlab.com/WPezPlugins/wpez-hubspot-newsletter-form-ajax-form
 * Description: The form - via shortcode - for the Hubspot Newsletter Form AJAX plugin.
 * Version: 0.0.0
 * Author: MF Simchock (Chief Executive Alchemist) at Alchemy United
 * Author URI: https://AlchemyUnited.com
 * License: GPLv2 or later
 * Text Domain: wpez-hnfa
 *
 * @package WPezHNFAF
 */

namespace WPezHNFAF;

add_shortcode( 'wpez_hsfa', __NAMESPACE__ . '\sccb_wpez_hsfa' );

/**
 * Shortcode callback.
 *
 * @param array  $atts SC atts.
 * @param string $content SC content (between the [SC]...content...[/SC] tags).
 * @param string $sc_name SC name.
 *
 * @return string
 */
function sccb_wpez_hsfa( string $atts, string $content = '', string $sc_name = 'wpez_hsfa' ) {

	$sc_atts = shortcode_atts(
		array(
			'id' => false,
		),
		$atts
	);

	ob_start();
	?>

	<p>Required fields are marked *</p>
	<form
	<?php
	if ( is_string( $sc_atts['id'] ) &&  ! empty( \esc_attr( $sc_atts['id'] ) ) ) {
		echo ' id="' . esc_attr( $sc_atts['id'] ) . '" ';
	}
	?>
	class="wpez-hsnf-form wpez-hsnf-step-1-true wpez-hsnf-step-2-false wpez-hsnf-step-3-false" method="post">

		<fieldset class="wpez-hsnf-step-1">
			<legend>Step 1 of 20<div>Your email address</div></legend>
				<div class="wpez-hsnf-fieldset-inner">
					<label for="wpez_hsnf_email" class="display-block">Email *</label>
					<input type="email" name="wpez_hsnf_email" class="wpez-hsnf-email" size="30" maxlength="245" required="required">
					<p class="wpez-hsnf-msg-step-1">A valid email address is required to continue.</p>
					<p class="wpez-hsnf-msg-step-1-true">Thank you. You're amazing.</p>
				</div>
		</fieldset>


		<div class="wpez-hsnf-step-2-wrapper">
			<fieldset class="wpez-hsnf-step-2">
				<legend>Step 2 of 2<div>Your consent, please</div></legend>
				<div class="wpez-hsnf-fieldset-inner">
					<p>Alchemy United is committed to protecting and respecting your privacy. We will not share your personal information with other third parties. For more details, please check the Privacy Policy page.</p>
					<p>This is a newsletter sign-up form. Naturally, this means AU will be using the details you submit to email you about news, products, services, inspiration, etc.</p>
					<p>Agreement to both of these is required.</p>
					<p>
						<input id="wpez_hsnf_legal_consent_process" type="checkbox" name="wpez_hsnf_legal_consent_process" value="true" data-hidden="true" required>
						<label for="wpez_hsnf_legal_consent_process">
							Yes, I agree to allow Alchemy United to store and process my personal data.
						</label>
					</p>
					<p>
						<input id="wpez_hsnf_legal_consent_communications" type="checkbox" name="wpez_hsnf_legal_consent_communications" value="true" data-hidden="true" required>
						<label for="wpez_hsnf_legal_consent_communications">
							Yes, I agree to receive newsletter communications from Alchemy United.
						</label>
					</p>
					<p class="wpez-hsnf-msg-step-2">You must agree to both conditions in order to continue.</p>
					<p class="wpez-hsnf-msg-step-2-true">Excellent! We're almost done here.
				</div>
			</fieldset>
		</div>

		<div class="wpez-hsnf-step-3-wrapper">
			<fieldset class="wpez-hsnf-step-3">
				<legend>The Bonus Round<div>100% Optional</div></legend>
				<div class="wpez-hsnf-fieldset-inner">
					<p>These are not required. However, kind and reasonable people - such as you? - who have come this far, tend to include them. Please?</p>

					<label for="wpez_hsnf_name_first" class="display-block">First Name</label>
					<input type="text"  name="wpez_hsnf_name_first" size="30" maxlength="245">

					<label for="wpez_hsnf_name_last" class="display-block">Last Name</label>
					<input type="text"  name="wpez_hsnf_name_last" size="30" maxlength="245">

					<p class="wpez-hsnf-msg-step-3-init">Your name? Sure, it's optional. But it's so easy.</p>
					<p class="wpez-hsnf-msg-step-3-thx-nf">Thank you. And your last name?</p>
					<p class="wpez-hsnf-msg-step-3-thx-nl">Thank you. You skipped your first name?</p>
					<p class="wpez-hsnf-msg-step-3-thx-both">Wow! Look at that. You're too kind.</p>
					<p class="wpez-hsnf-msg-step-3-thx-none">Ouch. That hurts. Try again?</p>
				</div>
			</fieldset>
			<div class="wpez-hsnf-step-3-msgs submit-false">
				<p>Sending...One moment please...</p>
			</div>
			<button type="submit" disabled>Sign Me Up</button>
		</div>
		<!-- Adds the form version to the HS sign up profile for future reference -->
		<input type="hidden" name="wpez_hsnf_form_version" value="TODO">
	</form>

	<?php
	$hsnf = ob_get_contents();
	ob_end_clean();

	return $hsnf;

}
